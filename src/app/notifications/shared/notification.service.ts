import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  notifications: String[] = [];

  constructor() { }

  add(notification: string) {
    this.notifications.push(notification);
  }
  
  remove(index: number) {
    if (index > -1) {
      this.notifications.splice(index);
    }
  }

  clear() {
    this.notifications = [];
  }
}
