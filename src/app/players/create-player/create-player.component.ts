import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../shared/player.service';
import { Player } from '../shared/player.model';

import { Location } from '@angular/common';

@Component({
  selector: 'app-create-player',
  templateUrl: './create-player.component.html',
  styleUrls: ['./create-player.component.css']
})
export class CreatePlayerComponent implements OnInit {

  isBusy: Boolean = false;

  player: Player = new Player();

  constructor(
    private location: Location,
    private playerService: PlayerService) { }

  ngOnInit() {
  }

  addPlayer(): void {
    this.isBusy = true;
    this.playerService.addPlayer(this.player)
      .subscribe(result => {
        this.location.back();
        this.player = new Player();
        this.isBusy = false;
      },
      error => {
        
      }
    );
  }

  onSubmit(): void {
    this.addPlayer();
  }
}
