import { Component, OnInit } from '@angular/core';
import { PlayerService } from './shared/player.service';
import { Player } from './shared/player.model';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  isBusy: Boolean = false;
  players: Player[];

  constructor(private playerService: PlayerService) { }

  ngOnInit() {
    this.getPlayers();
  }

  getPlayers(): void {
    this.isBusy = true;
    this.playerService.getPlayers()
      .subscribe(players => {
        this.players = players;
        this.sort();
        this.isBusy = false;
      });
  }

  addMatch(player: Player): void {
    this.isBusy = true;
    this.playerService.addMatch(player)
      .subscribe(p => {
        player.matches = p.matches;
        this.sort();
        this.isBusy = false;
      });
  }

  addWin(player: Player): void {
    this.isBusy = true;
    this.playerService.addWin(player)
      .subscribe(p => {
        this.isBusy = false;
        player.wins = p.wins;
        this.sort();
      });
  }

  sort(): void {
    this.players.sort(function(a: Player, b: Player) {
      return b.wins - a.wins;
    });
  }
}
