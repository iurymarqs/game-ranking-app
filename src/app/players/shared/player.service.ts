import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Player } from './player.model';
import { NotificationService } from '../../notifications/shared/notification.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(
    private notificationService: NotificationService,
    private http: HttpClient) { }

  private playersUrl = 'http://localhost:8080/players';

  getPlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(this.playersUrl)
      .pipe(
        catchError(this.handleError('getPlayers', []))
      );
  }

  addPlayer(player: Player) {
    return this.http.post<Player>(this.playersUrl, player, httpOptions).pipe(
      catchError(this.handleError<Player>('addPlayer'))
    );
  }

  addMatch(player: Player): Observable<Player> {
    return this.http.put<Player>(`${this.playersUrl}/${player.id}/addMatches`, null, httpOptions).pipe(
      catchError(this.handleError<Player>('addMatch'))
    );
  }

  addWin(player: Player): Observable<Player> {
    return this.http.put<Player>(`${this.playersUrl}/${player.id}/addWins`, null, httpOptions).pipe(
      catchError(this.handleError<Player>('addWin'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error.error);
      this.notificationService.add(error.error);
      return of(result as T);
    };
  }
}
